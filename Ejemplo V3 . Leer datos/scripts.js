const indexedDB = window.indexedDB
const form = document.getElementById('form')
const tareas = document.getElementById('tareas')
if (indexedDB && form) {
    let db
    const request = indexedDB.open('Listadetarea', 1)

    // Evento DOM EXITOSO
    request.onsuccess = () => {
        db = request.result
        console.log('OPEN', db)
        readData()
    }

    request.onupgradeneeded = (e) => {
        db = request.result
        console.log('Create', db)
        const objectStore = db.createObjectStore('tareas', {
            autoIncrement: true

        })
    }
    // Evento DOM ERROR
    request.onerror = (error) => {
        db = request.result
        console.log('Error', error)
    }
    // Funcion para agregar los datos 
    const addData = (data) => {
        const transaction = db.transaction(['tareas'], 'readwrite')
        const objectStore = transaction.objectStore('tareas')
        const request = objectStore.add(data)
        readData()
    }
    // Funcion para leer los datos
    const readData = () => {
        const transaction = db.transaction(['tareas'], 'readonly')
        const objectStore = transaction.objectStore('tareas')
        const request = objectStore.openCursor()
        const fragment = document.createDocumentFragment()

        request.onsuccess = (e) => {
            const cursor = e.target.result
            if (cursor) {

                const taskTitulo = document.createElement('P')
                taskTitulo.textContent = cursor.value.taskTitulo
                fragment.appendChild(taskTitulo)
                const taskPrioridad = document.createElement('P')
                taskPrioridad.textContent = cursor.value.taskPrioridad
                fragment.appendChild(taskPrioridad)
                cursor.continue()
            } else {
                tareas.textContent = ''
                tareas.appendChild(fragment)
            }
        }

    }

    form.addEventListener('submit', (e) => {
        e.preventDefault()
        const data = {
            taskTitulo: e.target.tarea.value,
            taskPrioridad: e.target.prioridad.value
        }
        addData(data)
    })
}