const indexedDB = window.indexedDB
const form = document.getElementById('form')
if (indexedDB && form) {
    let db
    const request = indexedDB.open('Listadetarea', 1)

    request.onsuccess = () => {
        db = request.result
        console.log('OPEN', db)
    }

    request.onupgradeneeded = (e) => {
        db = request.result
        console.log('Create', db)
        const objectStore = db.createObjectStore('tareas', {
            autoIncrement: true
        })
    }

    request.onerror = (error) => {
        db = request.result
        console.log('Error', error)
    }

    const addData = (data) => {
        const transaction = db.transaction(['tareas'], 'readwrite')
        const objectStore = transaction.objectStore('tareas')
        const request = objectStore.add(data)
    }

    form.addEventListener('submit', (e) => {
        e.preventDefault()
        const data = {
            taskTitulo: e.target.tareas.value,
            taskPrioridad: e.target.prioridad.value
        }
        addData(data)
    })
}