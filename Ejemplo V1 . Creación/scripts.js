const indexedDB = window.indexedDB

if (indexedDB) {
    let db
    const request = indexedDB.open('Listadetarea', 1)

    request.onsuccess = () => {
        db = request.result
        console.log('OPEN', db)
    }

    request.onupgradeneeded = () => {
        db = request.result
        console.log('Create', db)
        const objectStore = db.createObjectStore('tareas')
    }

    request.onerror = () => {
        db = request.result
        console.log('Error', error)
    }
}